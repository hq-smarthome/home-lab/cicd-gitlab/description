# GitLab CICD - Description

Repo for common documentation and setup information regarding CICD using GitLab within the hQ cluster

[[_TOC_]]

## Custom Executor

### Basic Structure

The 3 basic components of how GitLabs CICD system works with a cutom executor is as follows:

```mermaid
graph TD
    GL[GitLab]
    GLR[Runner]
    CE[Custom Executor]

    GLR -.->|Runner Registration| GL
    GLR -.->|Job logs| GL
    GL -->|Submit Job| GLR

    GLR -->|config_exec| CE
    GLR -->|prepare_exec| CE
    GLR -->|run_exec| CE
    GLR -->|clean_exec| CE
    CE -.->|script logs| GLR
```

where `config_exec`, `prepare_exec`, `run_exec`, `clean_exec` are shell scripts that the runner calls to interact with the custom executor.
The custom executor is a catch all for a custom runtime environment, In the case here the custom executor is a dispatched job running in a docker continer within Nomad.

More info on the custom executor can be found on the [GitLab Docs - Custom Executor](https://docs.gitlab.com/runner/executors/custom.html) page.

### Job Sequence

The following is a rough sequence of events that occur when a job is submitted to be executed. The custom executor in this case is a dispatched job within the Nomad cluster, the information and configuration for which can be found at the following:
- [CICD Gitlab - Executor Common Docker Image](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-docker)

- [CICD Gitlab - Executor Linux AMD64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-amd64)
- ~~[CICD Gitlab - Executor Linux ARM](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-arm)~~ (Planned in the future)
- ~~[CICD Gitlab - Executor Linux ARM64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-linux-arm64)~~ (Planned in the future)
- ~~[CICD Gitlab - Executor Darwin AMD64](https://gitlab.com/hq-smarthome/home-lab/cicd-gitlab/executor-darwin-amd64)~~ (Planned in the future)

```mermaid
sequenceDiagram
    participant GL as GitLab
    participant GLR as GitLab Runner
    participant N as Nomad
    participant CE as Custom Executor

    GL->>+GLR: Submit Job

    GLR->>+GLR: Run 'config_exec'

    opt
        GLR->>-GLR: Handle non-zero status code from  'config_exec'
    end
    GLR-->>GL: Reports executor name & version

    GLR->>+GLR: Run 'prepare_exec'

    GLR->>+N: Dispatch Executor Job
    N->>+CE: Starts allocation on cluster
    N-->>-GLR: Accepted Job Dispatch 

    loop
        GLR->>+N: Check Allocation Status
        N-->>-GLR: Report 'Pending' Allocation Status 
    end
    Note over N,GLR: Until CE is running or timeout
    CE->>-N: Healthcheck Running

    GLR->>+N: Check Allocation Status
    N-->>-GLR: Report 'Running' Allocation Status 

    opt
        GLR->>-GLR: Handle non-zero status code from  'prepare_exec'
    end
    GLR-->>GL: Reports status of prepare

    loop For each step within a job
        GLR->>+GLR: Run 'run_exec'

        GLR->>+N: nomad exec
        N->>+CE: Run command in allocation
        CE-->>-N: Return output and status code
        N-->>-GLR: Return output and status code
        GLR-->>GL: Output sent for viewing in UI

        opt
            GLR->>-GLR: Handle non-zero status code from 'run_exec'
        end
    end

    GLR->>+GLR: Run 'clean_exec'

    GLR->>+N: stop allocation
    N->>+CE: SIGTERM
    CE-->>-N: terminates
    N-->>-GLR: Allocation Stopped


    opt
        GLR->>-GLR: Handle response from 'clean_exec'
    end
    GLR-->>GL: Reports status of clean
    GLR-->>-GL: Job Finished

```
